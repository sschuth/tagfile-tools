/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Sebastian Schuth
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.blueelk.tagfiletools.mojo;

import de.blueelk.tagfiletools.Linter;
import de.blueelk.tagfiletools.LinterConfigurationEventListener;
import de.blueelk.tagfiletools.LinterConfigurator;
import de.blueelk.tagfiletools.PropertiesBasedLinterConfigurator;
import de.blueelk.tagfiletools.rule.Rule;
import de.blueelk.tagfiletools.rule.ViolationDescription;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.shared.model.fileset.FileSet;
import org.apache.maven.shared.model.fileset.util.FileSetManager;
import org.slf4j.impl.StaticLoggerBinder;

import java.io.File;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Checks a given list of tagfiles to match configurable rules.
 */
@Mojo(name = "lint")
public class LintMojo extends AbstractMojo {

    /**
     * Common base dir for tag files. Used to support rule exclusion by using globs.
     * To exclude a rule for one or more tag files, you must configure it to do so. See the rules documentation for
     * documentation on how to exclude a rule for a set of files.
     */
    @Parameter
    private File baseDir;

    /**
     * A list of <code>fileSet</code> rules to select the tag files to lint.
     */
    @Parameter
    private List<FileSet> tagfiles;

    /**
     * Configuration for linter rules.
     */
    @Parameter
    private Properties rulesConfiguration;
    private LinterConfigurationEventListener linterConfigurationEventListener;


    public void execute() throws MojoExecutionException, MojoFailureException {
        if (null == rulesConfiguration) {
            throw new MojoFailureException("Please configure the tagfile lint plugin.");
        }
        int numberOfViolations = 0;
        StaticLoggerBinder.getSingleton().setMavenLog(getLog());

        Linter linter = configuredLinter(rulesConfiguration);

        if (getLog().isDebugEnabled()) {
            for (Rule rule : linter.getRules()) {
                getLog().debug(rule.toString());
            }
        }

        // Find files to lint
        List<URI> tagfiles = findFiles(this.tagfiles);

        getLog().info("Checking " + tagfiles.size() +
                " tag file" + (tagfiles.size() != 1 ? "s" : "") +
                " using " + linter.numberOfRules() +
                " rule" + (linter.numberOfRules() != 1 ? "s" : "") + ".");

        // Lint files
        List<ViolationDescription> violations = linter.findViolations(tagfiles);

        // Let build succeed/fail
        Log log = getLog();
        if (!violations.isEmpty()) {
            numberOfViolations += violations.size();
            for (ViolationDescription violationDescription : violations) {
                log.warn(violationDescription.toString());
            }
        }
        getLog().info("Found "+numberOfViolations+" violations.");
        if (linter.isFatalViolationFound()) {
            throw new MojoFailureException("Fatal rule violations found.");
        } else {
            getLog().info("No fatal rule violations.");
        }
    }

    private Linter configuredLinter(Properties rulesConfiguration) {
        Linter linter = new Linter();
        linter.setBaseDir(baseDir);
        // Configure linter
        LinterConfigurator linterConfiguration = new PropertiesBasedLinterConfigurator(rulesConfiguration);
        linterConfigurationEventListener = new LinterConfigurationEventListener() {
            public void unknownRuleName(String ruleName, String className) {
                getLog().error("Unknown  rule '" + ruleName + "', i have searched for a class named '" + className + "')");
            }

            public void ruleException(String message) {
                getLog().error("An exception occurred when configuring rule '" + message + "'");
            }

            public void unknownProperty(String propertyName, String propertyValue) {
                getLog().error("Attempted to configure unknown property '" + propertyName + "' with value '" + propertyValue + "'");
            }

            public void unknwonValue(String propertyName, String propertyValue) {
                getLog().error("Attempted to configure property '" + propertyName + "' with wrong value '" + propertyValue + "'");
            }
        };
        linterConfiguration.configure(linter, linterConfigurationEventListener);
        return linter;
    }

    private List<URI> findFiles(List<FileSet> filesets) {
        List<URI> files = new ArrayList<URI>();
        FileSetManager fileSetManager = new FileSetManager();
        for (FileSet fileSet : filesets) {
            for (String fileName : fileSetManager.getIncludedFiles(fileSet)) {
                files.add(new File(fileSet.getDirectory(), fileName).toURI());
            }
        }
        return files;
    }


    public List<FileSet> getTagfiles() {
        return tagfiles;
    }

    public Properties getRulesConfiguration() {
        return rulesConfiguration;
    }
}
