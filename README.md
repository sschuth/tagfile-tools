Tagfile Tools
=============

Tagfile Tools aims to bring you some tooling for the development of large libraries of reusable components using tag files.

Tools provided by this project
------------------------------

For now, the main tool provided is a maven plugin allowing you to _lint_ tag files to make sure they conform to a coding standard.


Contributing
============

I welcome any contribution to this project - ideas, code, tests, bug reports... let it come!


About
=====

This project has sprung into existence out of the need for some tooling to get hold on problems when growing a large code base quickly and the quality problems a quickly growing code base sometimes has, combined with the quite poorly populated tool landscape for tag files.

The tools are (c) Sebastian Schuth