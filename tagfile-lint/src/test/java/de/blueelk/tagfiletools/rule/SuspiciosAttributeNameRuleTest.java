/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Sebastian Schuth
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.blueelk.tagfiletools.rule;

import de.blueelk.tagfiletools.directive.AttributeDirective;
import de.blueelk.tagfiletools.directive.Directive;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Arrays;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class SuspiciosAttributeNameRuleTest {

    private SuspiciousAttributeNameRule underTest;

    @BeforeMethod
    public void setup() {
        underTest = new SuspiciousAttributeNameRule();
    }

    @DataProvider
    public Object[][] attributeNames() {
        return new Object[][]{
                new Object[]{"className", false},
                new Object[]{"css", false},
                new Object[]{"cssClass", false},
                new Object[]{"foo", true},
        };
    }


    @Test(dataProvider = "attributeNames")
    public void marksAttributeAsSuspicious(String attributeName, boolean complies) {
        boolean result = underTest.complies(null, Arrays.asList(attributeDirective(attributeName)));
        assertThat("Expecting attribute name '" + attributeName + "'" + (!complies ? "" : " NOT") + " to be considered suspicious.", result, is(complies));
    }

    private Directive attributeDirective(String name) {
        final AttributeDirective attributeDirective = new AttributeDirective();
        attributeDirective.setName(name);
        return attributeDirective;
    }
}
