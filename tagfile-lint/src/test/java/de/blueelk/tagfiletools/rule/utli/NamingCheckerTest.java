/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Sebastian Schuth
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.blueelk.tagfiletools.rule.utli;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class NamingCheckerTest {

    private NamingChecker underTest;

    @BeforeTest
    private void setup() {
        underTest = new NamingChecker();
    }

    @DataProvider(name = "snakeCaseTests")
    public Object[][] snakeCaseData() {
        return new Object[][]{
                new Object[]{"Simple snake case is detected.", "snake_case", true},
                new Object[]{"CamelCase is rejected.", "camelCase", false},
                new Object[]{"Any uppercase char is rejected", "Snake_Case", false},
                new Object[]{"Any uppercase char is rejected", "Snake_Case", false},
                new Object[]{"Empty strings are snake case", "", true},
        };
    }

    @Test(dataProvider = "snakeCaseTests")
    public void detectsSnakeCase(String description, String example, boolean validOrNot) {
        assertThat(description, underTest.isSnakeCase(example), is(validOrNot));
    }


    @DataProvider(name = "camelCaseTests")
        public Object[][] camelCaseData() {
            return new Object[][]{
                    new Object[]{"Simple camel case is detected.", "camelCase", true},
                    new Object[]{"Camel case needs strictly lowercase after uppercase", "camelCaseURL", false},
                    new Object[]{"Camel case always starts with lowercase char", "CamelCase", false},
                    new Object[]{"Camel case may end with uppercase char", "camelCaseS", true},
                    new Object[]{"Camel case needs strictly lowercase after uppercase", "camelCaseUrl", true},
                    new Object[]{"All caps is no camel case.", "CAMELCASE", false},
                    new Object[]{"snake_case is no camel case.", "snake_case", false},
                    new Object[]{"Empty strings are camel case", "", true},

            };
        }

    @Test(dataProvider = "camelCaseTests")
    public void detectsCamelCase(String description, String example, boolean camelCaseOrNot){
        assertThat(description,underTest.isCamelCase(example),is(camelCaseOrNot));

    }



}
