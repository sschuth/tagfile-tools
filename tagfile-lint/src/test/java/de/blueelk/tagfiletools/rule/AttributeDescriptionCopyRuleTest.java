/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Sebastian Schuth
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.blueelk.tagfiletools.rule;

import de.blueelk.tagfiletools.directive.AttributeDirective;
import de.blueelk.tagfiletools.directive.Directive;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Arrays;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class AttributeDescriptionCopyRuleTest {

    private AttributeDescriptionCopyRule underTest;

    @BeforeMethod
    public void setup() {
        underTest = new AttributeDescriptionCopyRule();
    }

    @Test
    public void findsCopiedDescriptions() {
        boolean result = underTest.complies(null, Arrays.asList(attributeDirective("foo"), attributeDirective("foo")));

        assertThat(result, is(false));
    }

    @Test
    public void ignoresNullDescriptions() {
        boolean result = underTest.complies(null, Arrays.asList(attributeDirective(null), attributeDirective(null)));

        assertThat(result, is(true));
    }

    @Test
    public void ignoresEmptyDescriptions() {
        boolean result = underTest.complies(null, Arrays.asList(attributeDirective(""), attributeDirective(""), attributeDirective("   ")));

        assertThat(result, is(true));
    }

    @Test
    public void cleansUpStateBetweenRuns() {
        underTest.complies(null, Arrays.asList(attributeDirective("foo")));
        boolean result = underTest.complies(null, Arrays.asList(attributeDirective("foo")));
        assertThat("Attributes from previous runs should be ignored", result, is(true));
    }

    @Test
    public void checkName(){
        System.out.println(underTest.getName());

    }

    private Directive attributeDirective(String description) {
        final AttributeDirective attributeDirective = new AttributeDirective();
        attributeDirective.setDescription(description);
        return attributeDirective;
    }
}
