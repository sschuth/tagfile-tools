package de.blueelk.tagfiletools.rule;


import de.blueelk.tagfiletools.directive.AttributeDirective;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class NoOptionalNumberAttributeRuleTest {

    private NoOptionalNumberAttributeRule underTest;

    @BeforeMethod
    private void setup() {
        underTest = new NoOptionalNumberAttributeRule();
    }

    @DataProvider
    public Object[][] numberTypes() {
        return new Object[][]{
                // type / optional / expected to confirm to rule / description
                new Object[]{"java.lang.String", false, true, "non-optional string attributes confirm to the rule"},
                new Object[]{"java.lang.String", true, true, "optional string attributes confirm to the rule"},
                new Object[]{"java.lang.Integer", false, true, "required integer attributes are allowed"},
                new Object[]{Integer.class.getName(), true, false, "optional integer attributes are not allowed"},
                new Object[]{Double.class.getName(), true, false, "optional double attributes are not allowed"},
                new Object[]{Short.class.getName(), true, false, "optional short attributes are not allowed"},
                new Object[]{Float.class.getName(), true, false, "optional float attributes are not allowed"},
                new Object[]{Character.class.getName(), true, false, "optional char attributes are not allowed"},
                new Object[]{Byte.class.getName(), true, false, "optional byte attributes are not allowed"},
        };
    }

    @Test(dataProvider = "numberTypes")
    public void checkType(String type, boolean optional, boolean confirms, String description) {
        AttributeDirective attributeDirective = new AttributeDirective();
        attributeDirective.setType(type);
        attributeDirective.setRequired(!optional);

        String result = underTest.violationForDirective(attributeDirective);

        if (confirms) {
            assertThat(description, result, is(nullValue()));
        } else {
            assertThat(description, result, is(not(nullValue())));
            assertThat(result, containsString(type));
        }
    }

}