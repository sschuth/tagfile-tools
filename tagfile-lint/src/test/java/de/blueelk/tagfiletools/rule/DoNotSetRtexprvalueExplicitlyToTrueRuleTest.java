package de.blueelk.tagfiletools.rule;

import de.blueelk.tagfiletools.directive.AttributeDirective;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

public class DoNotSetRtexprvalueExplicitlyToTrueRuleTest {

    private DoNotSetRtexprvalueExplicitlyToTrueRule underTest;
    private AttributeDirective attributeDirective;

    @BeforeMethod
    private void setup() {
        underTest = new DoNotSetRtexprvalueExplicitlyToTrueRule();
        attributeDirective = new AttributeDirective();
    }

    @Test
    public void settingRtexprvalueToTrueIsDEtectedAsViolation(){
        // note: setting attributeDirective.setRtexprvalue() is not sufficient bc this defaults to true, see
        // BasicAttribute#wasValueProvidedFor() on how to check if the user has provided a value for an attribute
        // of a directive
        attributeDirective.addAttributeValue("rtexprvalue","true");
        String result = underTest.violationForDirective(attributeDirective);
        assertThat(result,is(not(nullValue())));
    }

    @Test
    public void settingRtexprvalueToFalseIsValid(){
        attributeDirective.addAttributeValue("rtexprvalue","false");
        String result = underTest.violationForDirective(attributeDirective);
        assertThat(result,is(nullValue()));
    }

}