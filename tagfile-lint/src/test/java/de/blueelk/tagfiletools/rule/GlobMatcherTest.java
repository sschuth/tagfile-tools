/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Sebastian Schuth
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.blueelk.tagfiletools.rule;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class GlobMatcherTest {

    @DataProvider(name = "provideTestData")
    public Object[][] provideTestData() {
        return new Object[][]{
                new Object[]{"Simple file name matching", "*.jpg", "blah.jpg", true},
                new Object[]{"", "*.jpg", "blah.png", false},
                new Object[]{"", "**/*.jpg", "foo/blah.jpg", true},
                new Object[]{"", "**/*.jpg", "foo/blah.png", false},
                new Object[]{"", "**/abc/*.jpg", "dfghjk/abc/blah.jpg", true},
                new Object[]{"", "**/a*/*.jpg", "dfghjk/abc/blah.jpg", true},
                new Object[]{"", "foo/a*/*.jpg", "dfghjk/abc/blah.jpg", false},
                new Object[]{"", "foo/a*/*.jpg", "foo/abc/blah.jpg", true},
                new Object[]{"", "**/*.jpg", "foo/abc/blah.jpg", true},
                new Object[]{"** matches zero dirs, too.", "**/*.jpg", "blah.jpg", true},
                new Object[]{"", "**/*.???", "blah.jpg", true},
                new Object[]{"", "**/*.???", "blah.png", true},
                new Object[]{"", "**", "blah.jpg", true},
                new Object[]{"", "abc/*", "abc/blah.jpg", true},
                new Object[]{"", "abc/", "abc/blah.jpg", true},
                new Object[]{"Globs ending with / are interpreted like one ending with /**", "abc/", "abc/def/blah.jpg", true},
                new Object[]{"Dir wildcard at the end of glob", "abc/**", "abc/def/blah.jpg", true},
                new Object[]{"Dir wildcard after fixed dir matcher", "abc/d/**", "abc/d/blah.jpg", true},
                new Object[]{"Dir wildcard after fixed dir matcher", "abc/d/**", "abc/de/blah.jpg", false},
                new Object[]{"", "abc/**/foo/abc.jpg", "abc/def/foo/abc.jpg", true},
                new Object[]{"", "abc/**/foo/abc.jpg", "abc/def/foo/def.jpg", false},
                new Object[]{"", "abc/**/foo/a?c.jpg", "abc/def/foo/abc.jpg", true},
                new Object[]{"", "abc/**/foo/a?c.jpg", "abc/def/foo/aec.jpg", true},
                new Object[]{"? cannot be a dir separator", "abc/**/foo?a?c.jpg", "abc/def/foo/aec.jpg", false},
                new Object[]{"! marks an optional character", "abc/**/foo/a?c.jpg", "abc/def/foo/ae.jpg", false},
                new Object[]{"Dir wildcard within a glob matches 2 directories", "abc/**/foo/???.jpg", "abc/def/ghi/foo/abc.jpg", true},
                new Object[]{"Dir wildcard within a glob matches 0 directories", "abc/**/foo/???.jpg", "abc/foo/abc.jpg", true},
                new Object[]{"** matches everything", "**", "/Users/sschuth/projects/private/tagfile-lint/tagfilelint/target/test-classes/de/blueelk/tagfiletools/noDescription.tag", true},
                new Object[]{"**/* matches everything", "**/*", "/Users/sschuth/projects/private/tagfile-lint/tagfilelint/target/test-classes/de/blueelk/tagfiletools/noDescription.tag", true},
        };
    }

    @Test(dataProvider = "provideTestData")
    public void checkGlobMatcher(String description, String glob, String path, boolean exectedToBeIncluded) {
        GlobMatcher underTest = new GlobMatcher();
        underTest.setGlob(glob);
        boolean result = underTest.included(path);
        assertThat(description + "- Expected that '" + glob + "' does " + (exectedToBeIncluded ? "" : "NOT ") + "include the path '" + path + "'", result, is(exectedToBeIncluded));
    }


}
