/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Sebastian Schuth
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.blueelk.tagfiletools;

import de.blueelk.tagfiletools.rule.TagDirectiveRequiresDescriptionRule;
import de.blueelk.tagfiletools.rule.ViolationDescription;
import org.testng.annotations.Test;

import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;

@Test
public class LinterTest {
    public void findsViolationsForRules() throws URISyntaxException {
        Linter underTest = new Linter();
        underTest.addRule(new TagDirectiveRequiresDescriptionRule());
        List<ViolationDescription> result = underTest.findViolations(Arrays.asList(getClass().getResource("noDescription.tag").toURI()));
        assertThat(result, hasSize(1));
        System.out.println(result.get(0));
    }

    public void respectsExclusionsForRules() throws URISyntaxException {
        Linter underTest = new Linter();
        final TagDirectiveRequiresDescriptionRule tagDirectiveRequiresDescriptionRule = new TagDirectiveRequiresDescriptionRule();
        tagDirectiveRequiresDescriptionRule.setExcludedFiles("**/*");//exclude all files
        underTest.addRule(tagDirectiveRequiresDescriptionRule);
        List<ViolationDescription> result = underTest.findViolations(Arrays.asList(getClass().getResource("noDescription.tag").toURI()));
        assertThat(result.isEmpty(),is(true));
    }


}
