/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Sebastian Schuth
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.blueelk.tagfiletools;

import de.blueelk.tagfiletools.directive.Directive;
import de.blueelk.tagfiletools.parser.FileSource;
import de.blueelk.tagfiletools.parser.TagFileParser;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;

@Test
public class TagFileParserTest {

    @Test
    public void parsesOneLineAttributes() throws IOException, URISyntaxException {
        List<Directive> parsed = parseTagFile("oneLineAttributes.tag");
        assertThat(parsed, hasSize(6));
    }

    @Test
    public void parsesMultiLineAttributes() throws IOException, URISyntaxException {
        List<Directive> parsed = parseTagFile("multiLineAttributes.tag");
        assertThat(parsed, hasSize(5));
    }

    @Test
    public void canParseUnknownDeclarations() throws IOException, URISyntaxException {
        List<Directive> parsed = parseTagFile("unknownDeclarations.tag");
        assertThat(parsed, hasSize(2));
        assertThat(parsed.get(1).getDirectiveName(), is(equalTo("basic")));
    }

    @Test
    public void parserDetectsRightLineNumbers() throws IOException, URISyntaxException {
        List<Directive> parsed = parseTagFile("multiLineAttributes.tag");
        assertThat(parsed.get(0).getLineNumber(),is(equalTo(25)));
        assertThat(parsed.get(1).getLineNumber(),is(equalTo(27)));
        assertThat(parsed.get(2).getLineNumber(),is(equalTo(31)));
        assertThat(parsed.get(3).getLineNumber(),is(equalTo(34)));
        assertThat(parsed.get(4).getLineNumber(),is(equalTo(37)));

    }

    private List<Directive> parseTagFile(String fileName) throws URISyntaxException, IOException {
            URL tagResource = getClass().getResource(fileName);
            File simpleTag = new File(tagResource.toURI());
            TagFileParser underTest = new TagFileParser();
            return underTest.parse(new FileSource(simpleTag));
        }
}
