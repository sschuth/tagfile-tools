/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Sebastian Schuth
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Sebastian Schuth
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.blueelk.tagfiletools;

import de.blueelk.tagfiletools.rule.Rule;
import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Configures a linter using keys from an properties file.
 */
public class PropertiesBasedLinterConfigurator implements LinterConfigurator {

    final static Logger LOGGER = LoggerFactory.getLogger(PropertiesBasedLinterConfigurator.class);
    private final Properties configurationProperties;

    private LinterConfigurationEventListener linterConfigurationEventListener;
    private static final LinterConfigurationEventListener DEFAULT_EVENT_LISTENER = new LinterConfigurationEventListener() {
        @Override
        public void unknownRuleName(String ruleName, String className) {
            LOGGER.error("Unknown Rule: " + ruleName + "(" + className + ")");
        }

        @Override
        public void ruleException(String message) {
            LOGGER.error("Error when instantiating rule: " + message);
        }

        @Override
        public void unknownProperty(String propertyName, String propertyValue) {
            LOGGER.error("Unknown property: " + propertyName + "=" + propertyValue);
        }

        @Override
        public void unknwonValue(String propertyName, String propertyValue) {
            LOGGER.error("Unknown value: " + propertyName + "=" + propertyValue);
        }
    };

    private static final Pattern RULE_PATTERN = Pattern.compile("rule\\.([^\\.]+)\\.?(.+)");

    private List<Rule> rules = new ArrayList<Rule>();

    public PropertiesBasedLinterConfigurator(Properties configurationProperties) {
        this.configurationProperties = configurationProperties;
    }


    @Override
    public void configure(Linter linter, LinterConfigurationEventListener linterConfigurationEventListener) {
        this.linterConfigurationEventListener = linterConfigurationEventListener;
        rules.clear();
        createRules();
        for (Rule rule : rules) {
            linter.addRule(rule);
        }
    }

    private void createRules() {
        Map<String, List<String>> rules = new HashMap<String, List<String>>();
        for (String propertyName : configurationProperties.stringPropertyNames()) {
            Matcher matcher = RULE_PATTERN.matcher(propertyName);
            if (matcher.matches() && matcher.groupCount() == 2) {
                String ruleName = matcher.group(1);
                if (!rules.containsKey(ruleName)) {
                    rules.put(ruleName, new ArrayList<String>());
                }
                rules.get(ruleName).add(matcher.group(0));
            } else {
                LOGGER.debug("rule not matched: " + propertyName);
            }
        }
        for (String ruleName : rules.keySet()) {
            addBuiltInRule(ruleName, rules.get(ruleName), configurationProperties);
        }
    }

    @Override
    public void configure(Linter linter) {
        configure(linter, DEFAULT_EVENT_LISTENER);
    }

    private void addBuiltInRule(String ruleName, List<String> configurationKeys, Properties configurationProperties) {
        String className = "de.blueelk.tagfiletools.rule." + ruleName + "Rule";

        Class<Rule> ruleClass = null;
        try {
            ruleClass = (Class<Rule>) Class.forName(className);
        } catch (ClassNotFoundException e) {
            linterConfigurationEventListener.unknownRuleName(ruleName, className);
        }
        Rule rule = null;
        if (ruleClass != null) {
            try {
                rule = ruleClass.newInstance();
            } catch (InstantiationException e) {
                linterConfigurationEventListener.ruleException(e.getMessage());
            } catch (IllegalAccessException e) {
                linterConfigurationEventListener.ruleException(e.getMessage());
            }
        }
        if (rule == null) return;
        rule.setEnabled(true);
        configureRule(rule, configurationKeys, configurationProperties);
        rules.add(rule);
    }

    private void configureRule(Rule rule, List<String> configurationKeys, Properties configurationProperties) {
        for (String configurationKey : configurationKeys) {
            String propertyName = configurationKey.substring(configurationKey.lastIndexOf(".") + 1);
            String value = configurationProperties.getProperty(configurationKey);
            try {
                Map description = BeanUtils.describe(rule);
                if (!description.containsKey(propertyName)) {
                    linterConfigurationEventListener.unknownProperty(propertyName, value);
                }
                BeanUtils.setProperty(rule, propertyName, value);
            } catch (IllegalAccessException e) {
                linterConfigurationEventListener.ruleException(e.getMessage());
            } catch (InvocationTargetException e) {
                linterConfigurationEventListener.ruleException(e.getMessage());
            } catch (NoSuchMethodException e) {
                linterConfigurationEventListener.ruleException(e.getMessage());
            }
        }
    }

}