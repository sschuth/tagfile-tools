/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Sebastian Schuth
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.blueelk.tagfiletools.parser;

import de.blueelk.tagfiletools.directive.AttributeDirective;
import de.blueelk.tagfiletools.directive.BasicDirective;
import de.blueelk.tagfiletools.directive.Directive;
import de.blueelk.tagfiletools.directive.TagDirective;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TagFileParser {
    public static Logger LOGGER = LoggerFactory.getLogger(TagFileParser.class);

    private final Pattern DIRECTIVE_START = Pattern.compile("\\s*<%@\\s*(\\S+)(.*)");
    private final Pattern DIRECTIVE = Pattern.compile("\\s*<%@\\s*(\\S+)(.*)%>");
    private final Pattern ARGUMENT = Pattern.compile("^\\s*(\\S+)=\"([^\"]+)\"?\\s*$");

    public List<Directive> parse(TagFileSource source) throws IOException {
        int lineNumber = 0;
        int directiveStartLine = -1;
        List<Directive> directives = new ArrayList<Directive>();
        source.open();
        String lineBuffer = "";
        Directive currentDirective;
        for (String line : source) {
            lineNumber++;
            lineBuffer = lineBuffer + line;
            Matcher match = DIRECTIVE.matcher(lineBuffer);
            if (match.matches()) {
                String directiveName = match.group(1);
                currentDirective = getDirective(directiveName, directiveStartLine == -1 ? lineNumber : directiveStartLine);
                LOGGER.debug("Found a directive at line:" + lineNumber + ", directiveName: '" + directiveName + "' with argument string: '" + match.group(2) + "'");
                parseArgumentString(currentDirective, match.group(2));
                directives.add(currentDirective);
                lineBuffer = "";
                directiveStartLine = -1;
            } else {
                Matcher directiveStartMatch = DIRECTIVE_START.matcher(lineBuffer);
                if (!directiveStartMatch.matches()) {
                    LOGGER.debug("Skipping line " + lineNumber + ":" + lineBuffer);
                    lineBuffer = "";
                } else if (directiveStartLine == -1) {
                    directiveStartLine = lineNumber;
                }
            }
        }

        source.close();
        return directives;
    }

    private Directive getDirective(String directiveName, int lineNumber) {
        Directive directive;
        if ("attribute".equals(directiveName)) {
            directive = new AttributeDirective();
        } else if ("tag".equals(directiveName)) {
            directive = new TagDirective();
        } else {
            directive = new BasicDirective();
        }
        directive.setLineNumber(lineNumber);
        return directive;
    }

    private void parseArgumentString(Directive directive, String argumentString) {
        String[] arguments = argumentString.split("\"\\s");
        for (String argument : arguments) {
            Matcher argumentMatcher = ARGUMENT.matcher(argument);
            if (argumentMatcher.matches()) {
                String attributeName = argumentMatcher.group(1);
                String attributeValue = argumentMatcher.group(2);
                LOGGER.debug("\tfound argument:" + attributeName + "->" + attributeValue);
                directive.addAttributeValue(attributeName, attributeValue);
            }
        }
    }
}
