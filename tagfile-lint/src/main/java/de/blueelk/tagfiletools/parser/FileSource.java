/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Sebastian Schuth
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.blueelk.tagfiletools.parser;

import java.io.*;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class FileSource implements TagFileSource {

    private final File file;
    private BufferedReader reader;

    public FileSource(File file) {
        this.file = file;
    }

    @Override
    public void open() throws IOException {
        reader = new BufferedReader(new FileReader(file));
    }

    @Override
    public void close() throws IOException {
        if (reader != null) {
            reader.close();
            reader = null;
        }
    }

    public Iterator<String> iterator() {
        if (reader == null) {
            throw new IllegalStateException("Call open() before using the source.");
        }
        return new FileLineIterator(reader);
    }

    private class FileLineIterator implements Iterator<String> {
        private final BufferedReader br;
        private String nextLine;

        private FileLineIterator(BufferedReader reader) {
            this.br = reader;
        }

        public boolean hasNext() {
            ensureNextLine();
            return nextLine != null;
        }

        private void ensureNextLine() {
            if (nextLine == null) {
                try {
                    nextLine = br.readLine();
                } catch (IOException e) {
                    nextLine = null;
                }
            }
        }

        public String next() {
            ensureNextLine();
            if (nextLine == null) throw new NoSuchElementException();
            String toReturn = nextLine;
            nextLine = null;
            return toReturn;
        }

        public void remove() {
            throw new UnsupportedOperationException("Source is read-only.");
        }
    }
}
