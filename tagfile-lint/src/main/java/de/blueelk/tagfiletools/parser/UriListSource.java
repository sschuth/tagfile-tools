/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Sebastian Schuth
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.blueelk.tagfiletools.parser;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class UriListSource implements TagFileSource {

    private final List<URI> resources;
    private final List<FileSource> fileSources = new ArrayList<FileSource>();

    public UriListSource(List<URI> resources) {
        this.resources = Collections.unmodifiableList(resources);
    }

    @Override
    public void open() throws IOException {
        for (URI resource : resources) {
            FileSource fileSource = new FileSource(new File(resource));
            fileSources.add(fileSource);
            fileSource.open();
        }
    }

    @Override
    public void close() throws IOException {
        for (FileSource fileSource : fileSources) {
            fileSource.close();
        }
    }

    @Override
    public Iterator<String> iterator() {
        return new ChainedIterator();
    }

    private class ChainedIterator implements Iterator<String> {

        private final Iterator<FileSource> fileSourceIterator;
        private Iterator<String> currentIterator;

        private ChainedIterator() {
            this.fileSourceIterator = fileSources.iterator();
        }

        @Override
        public boolean hasNext() {
            if (null == currentIterator) {
                getNextIterator();
            }
            if (null == currentIterator) {
                return false;
            }
            return currentIterator.hasNext() || hasNext();
        }

        private void getNextIterator() {
            if (fileSourceIterator.hasNext()) {
                currentIterator = fileSourceIterator.next().iterator();
            } else {
                currentIterator = null;
            }
        }

        @Override
        public String next() {
            return currentIterator.next();
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException("Source is read-only.");
        }
    }

}
