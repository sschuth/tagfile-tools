/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Sebastian Schuth
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.blueelk.tagfiletools.directive;

public class AttributeDirective extends BasicDirective {


    private String name;
    private String description = "";
    private String type = "java.lang.String";
    private boolean required = false;
    private boolean rtexprvalue = true;
    private boolean fragment = false;

    @Override
    public String getDirectiveName() {
        return "attribute";
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getType() {
        return type;
    }

    public boolean isRequired() {
        return required;
    }

    public boolean isRtexprvalue() {
        return rtexprvalue;
    }

    public boolean isFragment() {
        return fragment;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public void setRtexprvalue(boolean rtexprvalue) {
        this.rtexprvalue = rtexprvalue;
    }

    public void setFragment(boolean fragment) {
        this.fragment = fragment;
    }

    @Override
    public String toString() {
        return "AttributeDirective{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", type='" + type + '\'' +
                ", required=" + required +
                ", rtexprvalue=" + rtexprvalue +
                ", fragment=" + fragment +
                '}';
    }
}
