/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Sebastian Schuth
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.blueelk.tagfiletools.directive;

public interface Directive {

    /**
     * Get the unique name of this directive.
     * @return The unique name of this directive.
     */
    String getDirectiveName();

    /**
     * Line number at which this directive started within teh source file defining it.
     *
     * NOTE: this value does NOT represent an index, so the first lien will have teh number 1.
     *
     * @return The line number.
     */
    int getLineNumber();

    /**
     * Sets the line number at which the declaration of this directive has stated within the source file.
     *
     * NOTE: the first line of a file has the line number 1, NOT 0.
     *
     * @param lineNumber The line number.
     */
    void setLineNumber(int lineNumber);

    /**
     * Adds the given name/value pair to the attributes defined for a directive.
     * @param name The name of the attribute.
     * @param value Value for the attribute as found in the source.
     */
    void addAttributeValue(String name, String value);

}
