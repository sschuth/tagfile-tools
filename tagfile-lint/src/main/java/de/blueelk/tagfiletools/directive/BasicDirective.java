/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Sebastian Schuth
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.blueelk.tagfiletools.directive;

import org.apache.commons.beanutils.BeanUtils;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

/**
 * Basic directive, may hold any attributes.
 * This class also serves as base class for more specialized types of directives.
 */
public class BasicDirective implements Directive {

    private Map<String, String> unknownAttributeValues = new HashMap<String, String>();
    private Map<String, String> allAttributeValuesSet = new HashMap<String, String>();
    private int lineNumber;

    @Override
    public String getDirectiveName() {
        return "basic";
    }

    @Override
    public void setLineNumber(int lineNumber) {
        this.lineNumber = lineNumber;
    }

    @Override
    public int getLineNumber() {
        return lineNumber;
    }

    @Override
    public void addAttributeValue(String name, String value) {
        try {
            BeanUtils.setProperty(this, name, value);
        } catch (IllegalAccessException e) {
            unknownAttributeValues.put(name, value);
            System.err.println(e);
        } catch (InvocationTargetException e) {
            unknownAttributeValues.put(name, value);
            System.err.println(e);
        }
        allAttributeValuesSet.put(name,value);
    }

    public boolean hasUnknownAttributes() {
        return !unknownAttributeValues.isEmpty();
    }

    public boolean wasValueProvidedFor(String attributeName){
        return allAttributeValuesSet.containsKey(attributeName);
    }

}
