/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Sebastian Schuth
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Sebastian Schuth
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package de.blueelk.tagfiletools;

import de.blueelk.tagfiletools.directive.Directive;
import de.blueelk.tagfiletools.parser.FileSource;
import de.blueelk.tagfiletools.parser.TagFileParser;
import de.blueelk.tagfiletools.rule.CheckExclusionSupport;
import de.blueelk.tagfiletools.rule.Rule;
import de.blueelk.tagfiletools.rule.ViolationDescription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

/**
 * Instances can check a list of tag files against rules.
 */
public class Linter {

    final static Logger LOGGER = LoggerFactory.getLogger(Linter.class);

    private List<Rule> rules = new ArrayList<Rule>();
    private TagFileParser parser = new TagFileParser();
    private File baseDir;
    private boolean fatalViolationFound=false;


    /**
     * Sets the tag file base directory used to decide if a rule is excluded by configuration.
     * @param baseDir Teh base directory for all tag files.
     *
     * @see CheckExclusionSupport
     */
    public void setBaseDir(File baseDir) {
        this.baseDir = baseDir;
    }

    /**
     * Add a rule to the linter.
     *
     * @param rule The rule to add.
     */
    public void addRule(Rule rule) {
        rules.add(rule);
    }

    /**
     * Get the number of rules a linter uses when checking tag files.
     *
     * @return The number of configured rules.
     */
    public int numberOfRules() {
        return rules.size();
    }

    /**
     * Gets the list of rules in the order they are used to check the files.
     *
     * @return The list of rules.
     */
    public List<Rule> getRules() {
        return rules;
    }

    public boolean isFatalViolationFound() {
        return fatalViolationFound;
    }

    /**
     * Checks the given tag files against all rules added by {@link #addRule(de.blueelk.tagfiletools.rule.Rule)} calls.
     *
     * @param tagfiles URIs pointing to the tag files to check.
     * @return A list of violations found for the tag files.
     */
    public List<ViolationDescription> findViolations(List<URI> tagfiles) {
        List<ViolationDescription> violationDescriptions = new ArrayList<ViolationDescription>();
        for (URI uri : tagfiles) {
            final File tagFile = new File(uri);
            FileSource fileSource = new FileSource(tagFile);
            String relativePath = getRelativePath(tagFile);
            try {
                List<Directive> directives = parser.parse(fileSource);
                final ViolationDescription violationDescription = checkRuleCompliance(uri, directives, relativePath);
                if (violationDescription.isViolationFound()) {
                    violationDescriptions.add(violationDescription);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return violationDescriptions;
    }

    private String getRelativePath(File tagFile) {
        String relativePath = tagFile.getAbsolutePath();
        if(null != baseDir){
            relativePath = relativePath.replace(baseDir.getAbsolutePath()+File.separator,"");
        }
        return relativePath;
    }


    private ViolationDescription checkRuleCompliance(URI resourceUri, List<Directive> directives, String relativePath) {
        ViolationDescription violationDescription = new ViolationDescription();
        for (Rule rule : rules) {
            violationDescription.setCurrentFileName(relativePath);
            LOGGER.debug("Relative path: " + relativePath);
            if (!(rule instanceof CheckExclusionSupport) || ((CheckExclusionSupport) rule).shouldCheck(relativePath)) {
                LOGGER.debug("Checking rule: " + rule);
                if (!rule.complies(resourceUri, directives)) {
                    if(rule.isFatal()) fatalViolationFound = true;
                    rule.describeTo(violationDescription);
                }
            }else{
                LOGGER.info("Skipped rule for tag file: " + relativePath);
            }
        }
        return violationDescription;
    }
}
