package de.blueelk.tagfiletools.rule;

import de.blueelk.tagfiletools.directive.AttributeDirective;

public class DoNotSetRtexprvalueExplicitlyToTrueRule extends ConfigurableAttributeRule {

    @Override
    public String violationForDirective(AttributeDirective attributeDirective) {
        if (attributeDirective.isRtexprvalue() && attributeDirective.wasValueProvidedFor("rtexprvalue")) {
            return "Do not set attribute 'rtexprvalue' explicitly to true - this is the default";
        }
        return null;
    }

    @Override
    protected void clean() {
        //nothing to clean up
    }
}
