package de.blueelk.tagfiletools.rule;

import de.blueelk.tagfiletools.directive.AttributeDirective;

import java.util.ArrayList;
import java.util.List;

/**
 * Finds optional attributes of a numeric type.
 */
public class NoOptionalNumberAttributeRule extends ConfigurableAttributeRule {

    static Class[] disallowedOptionalTypes = new Class[]{
            Byte.class, Integer.class, Double.class, Float.class, Short.class, Character.class
    };
    static final List<String> forbiddenOptionalAttributeValues = new ArrayList<String>();
    static {
        for (Class clazz : disallowedOptionalTypes) {
            forbiddenOptionalAttributeValues.add(clazz.getName());
        }
    }

    @Override
    public String violationForDirective(AttributeDirective attributeDirective) {
        if (!attributeDirective.isRequired() && forbiddenOptionalAttributeValues.contains(attributeDirective.getType())) {
            return "Attributes of type '" + attributeDirective.getType() + "' must not be optional.";
        }
        return null;
    }

    @Override
    protected void clean() {
        ///nothing to clean up
    }
}
