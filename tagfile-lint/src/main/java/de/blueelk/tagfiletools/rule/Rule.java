/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Sebastian Schuth
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Sebastian Schuth
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.blueelk.tagfiletools.rule;

import de.blueelk.tagfiletools.directive.Directive;

import java.net.URI;
import java.util.List;

/**
 * A rule checks directives and - in case of a rule violation - describes the violation to a given description.
 */
public interface Rule {

    /**
     * Check if the given list of directives comply to this rule.
     *
     * @param directives The list of directive to be checked.
     * @return <code>true</code> if the directives comply to this rule, <code>false</code> otherwise.
     */
    boolean complies(URI tagFileUri, List<Directive> directives);

    /**
     * Describes the violation using the {@link ViolationDescription} api.
     *
     * @param description The description used to describe the violation.
     */
    void describeTo(ViolationDescription description);

    /**
     * Sets if his rule is enabled.
     *
     * @param enabled If true, the rule is enabled and should be used for validations, if false the rule should be ignored.
     */
    void setEnabled(boolean enabled);

    /**
     * Checks if this rule is enabled.
     *
     * @return true if this rule is enabled and should be used for validation, false if it should not be used.
     */
    boolean isEnabled();

    /**
     * If true, a violation of this rule is considered fatal.
     *
     * @return true if violations are considered fatal.
     */
    boolean isFatal();

    /**
     * Sets if rule violations are considered fatal.
     *
     * @param fatal - if true, consider violations fatal.
     */
    void setFatal(boolean fatal);

    /**
     * Returns the name as used within configurations files.
     * @return The name of this rule as used in configuration files.
     */
    String getName();
}
