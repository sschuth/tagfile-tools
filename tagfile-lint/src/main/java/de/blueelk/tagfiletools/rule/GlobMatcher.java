/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Sebastian Schuth
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Sebastian Schuth
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.blueelk.tagfiletools.rule;

import java.util.regex.Pattern;

/**
 * Implements glob matching as describes for ant tasks at https://ant.apache.org/manual/dirtasks.html
 */
public class GlobMatcher {
    // matches N directories (glob:"**")
    public static final String ANY_NUMBERS_OF_DIRS = "([^//]*/)*";
    // matches every char except a dir separator
    public static final String ANYTHING_BUT_SEPARATORS = "[^/]";
    // The pattern being constructed by parsing the glob.
    private Pattern pattern;

    public GlobMatcher() {
        this("**");
    }

    public GlobMatcher(String glob) {
        this.setGlob(glob);
    }

    public boolean included(String path) {
        return pattern.matcher(path).matches();
    }

    public void setGlob(String glob) {
        // normalize globs ending with "/"
        if (glob.endsWith("/")){
            glob = glob + "**";
        }
        String[] globParts = glob.split("/");
        StringBuilder patternBuilder = new StringBuilder();
        if (globParts.length > 1) {
            for (int i = 0; i < globParts.length - 1; i++) {
                if ("**".equals(globParts[i])) {
                    patternBuilder.append(ANY_NUMBERS_OF_DIRS);
                } else {
                    createRegexpForPart(globParts[i], patternBuilder);
                    if (i + 1 < globParts.length) patternBuilder.append("/");
                }
            }
        }

        final String lastPart = globParts[globParts.length - 1];
        if ("**".equals(lastPart)) {
            patternBuilder.append(".*");
        } else {
            createRegexpForPart(lastPart, patternBuilder);
        }
        this.pattern = Pattern.compile(patternBuilder.toString());
    }

    /**
     * Creates the regexp part for path parts that are not multiple-globPart globs ("**").
     * @param globPart The directory pattern within the glob
     * @param patternBuilder The builder used to construct the regexp expression.
     */
    private void createRegexpForPart(String globPart, StringBuilder patternBuilder) {
        patternBuilder.append(globPart.replace("?", ANYTHING_BUT_SEPARATORS + "?").replace("*", ANYTHING_BUT_SEPARATORS + "*"));
    }
}
