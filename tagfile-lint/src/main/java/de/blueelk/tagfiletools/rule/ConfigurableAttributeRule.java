/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Sebastian Schuth
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Sebastian Schuth
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.blueelk.tagfiletools.rule;

import de.blueelk.tagfiletools.directive.AttributeDirective;
import de.blueelk.tagfiletools.directive.Directive;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class ConfigurableAttributeRule extends ConfigurableRule {

    private final Logger logger;
    private List<String> violations = new ArrayList<String>();
    private String excludedAttributeNames = "";

    protected ConfigurableAttributeRule() {
        logger = LoggerFactory.getLogger(this.getClass());
    }

    @Override
    public boolean complies(URI tagFileUri, List<Directive> directives) {
        violations.clear();
        clean();
        List<String> excluded = Arrays.asList(excludedAttributeNames.split("/"));
        for (Directive directive : directives) {
            if (directive instanceof AttributeDirective) {
                AttributeDirective attributeDirective = (AttributeDirective) directive;
                if (excluded.contains(attributeDirective.getName())) {
                    logger.info("Skipping attribute with name=" + attributeDirective.getName());
                } else {
                    final String violation = violationForDirective(attributeDirective);
                    if (null != violation) {
                        violations.add("[Line " + directive.getLineNumber() + "] " + violation);
                    }
                }
            }
        }
        return violations.isEmpty();
    }

    @Override
    public void describeTo(ViolationDescription description) {
        for (String violation : violations) description.addViolation(violation, isFatal(), getName());
    }

    /**
     * Set the comma separated list of attribute names that should not be checked.
     *
     * @param excludedAttributeNames Comma-separated list of attribute names not to be checked by this rule.
     */
    public void setExcludedAttributeNames(String excludedAttributeNames) {
        this.excludedAttributeNames = excludedAttributeNames;
    }

    /**
     * Get the comma separated list of attribute names that should not be checked.
     * @return List containing the names of all excluded attributes.
     */
    public String getExcludedAttributeNames() {
        return excludedAttributeNames;
    }


    /**
     * Checks the given attribute directive for a violation.
     *
     * @param attributeDirective The directive to check.
     * @return null if no violation was found, a description string for the violation otherwise.
     */
    public abstract String violationForDirective(AttributeDirective attributeDirective);

    /**
     * Called to inform the rule about a new set of directives being sent for usage. Can be used to clean up state.
     */
    protected abstract void clean();


}
