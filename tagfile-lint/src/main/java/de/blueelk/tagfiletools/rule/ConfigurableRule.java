/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Sebastian Schuth
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Sebastian Schuth
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.blueelk.tagfiletools.rule;

/**
 * Provides support for some features supported by the {@link Rule} interface.
 * <ul>
 * <li>enabled/disabled</li>
 * <li>fatal/non-fatal</li>
 * <li>file exclusion support</li>
 * </ul>
 */
public abstract class ConfigurableRule implements Rule, CheckExclusionSupport {

    private boolean enabled = false;
    private boolean fatal = false;
    private GlobMatcher excludedFilesMatcher;
    private String excludedFiles;

    @Override
    public boolean shouldCheck(String relativeFilePath) {
        return null == excludedFilesMatcher || !excludedFilesMatcher.included(relativeFilePath);
    }

    @Override
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    @Override
    public boolean isFatal() {
        return fatal;
    }

    @Override
    public void setFatal(boolean fatal) {
        this.fatal = fatal;
    }

    public void setExcludedFiles(String excludedFilesGlob) {
        this.excludedFilesMatcher = new GlobMatcher(excludedFilesGlob);
        this.excludedFiles = excludedFilesGlob;
    }

    public String getExcludedFiles() {
        return excludedFiles;
    }

    @Override
    public String getName() {
        return (this.getClass().getCanonicalName().substring(this.getClass().getCanonicalName().lastIndexOf('.') + 1)).replace("Rule", "");
    }

    @Override
    public String toString() {
        return "ConfigurableRule{" +
                "enabled=" + enabled +
                ", fatal=" + fatal +
                ", excludedFiles='" + excludedFiles + '\'' +
                '}';
    }
}
