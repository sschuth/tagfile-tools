/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Sebastian Schuth
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Sebastian Schuth
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.blueelk.tagfiletools.rule;

import java.util.ArrayList;
import java.util.List;

/**
 * Describes violations found within a single resource.
 */
public class ViolationDescription {

    private String currentFileName;
    private List<Violation> violations = new ArrayList<Violation>();


    public void setCurrentFileName(String fileName) {
        this.currentFileName = fileName;
    }

    public ViolationDescription addViolation(String violationDescription, boolean fatal, String ruleName) {
        violations.add(new Violation(currentFileName, violationDescription, fatal, ruleName));
        return this;
    }

    public boolean isViolationFound() {
        return !violations.isEmpty();
    }

    private class Violation {
        final boolean fatal;
        final String ruleName;
        final String fileName, violation;

        Violation(String fileName, String violation, boolean fatal, String ruleName) {
            this.fileName = fileName;
            this.violation = violation;
            this.fatal = fatal;
            this.ruleName = ruleName;

        }

        @Override
        public String toString() {
            return (!fatal ? "(NON-" : "(") + "FATAL)\tfilename: " + fileName + ", ruleName:" + ruleName + ", violation: '" + violation + "'";
        }
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder("\n");
        for (int i = 0; i < violations.size(); i++) {
            Violation violation = violations.get(i);
            stringBuilder.append(" ").append(violation.toString());
            if (i != violations.size() - 1) stringBuilder.append("\n");
        }
        return stringBuilder.toString();
    }
}
