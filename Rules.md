Rules for the tagfile linter
============================

All rules share two flags: `enabled` and `fatal`.

If a rule is configured with `fatal=true`, a violation of the rule should make the build fail.

Additionally, you can exclude files from rules by specifying a glob pattern. This is done using
the `excludedFiles` property. The glob follows the rules for Ant globs.


Tag Description Rule
--------------------

**Name**: TagDirectiveRequiresDescription

**Options**: none

This rule ensures that a tag file contains at least one tag directive with a non-empty description attribute.


Attribute Description Rule
--------------------------

**Name**: AttributeDescription

**Options**: none

This rule ensures that every attribute directive has a non-empty description attribute.

Attribute Description Copies Rule
---------------------------------

**Name**: AttributeDescriptionCopy
**Options**:

* *excludedAttributeNames*: comma-separated list of attribute names to be ignored for this rule


This rule ensures that there are no two attributes within a single tag file having the same description, which is often
the case if attribute declarations are copied. *Empty descriptions are ignored and can be found by the Attribute
Description Rule*.

Suspicious Attribute Name Rule
------------------------------

**Name**: SuspiciousAttributeName

**Options**:
* *suspiciousNamePattern* : regexp to look for to identify suspicious attribute names. Default: "[css|className|cssClass]"

This rule can be used to find attributes with names that may be forbidden, such as passing css class names to a tag
file. This rule can only warn of suspicious names, the real usage of attributes is not analyzed.

Attribute Naming Rule
----------------------

(NYI)

**Name**: AttributeNaming

**Options**:
    *namingStrategy* : "camelCase" "snake_case"

This rule can find attributes that do not follow common naming strategies such as camelCase names by forbidding certain
characters within attribute names.

As using hyphens within attribute names is considered a bad practice, there is NO "hyphen-case" support.


Tag File Naming Rule
--------------------

(NYI)

**Name:**: TagFileNaming

**Options**
    *namingStrategy* : "camelCase", "snake_case", "camelCase_mixedWithSnake", "hyphen-case"

This rule can be used to enforce a certain rule for naming tag files, such as using only underscores and characters.

Attribute Required value rule
----------------------------

(NYI)

This rule ensures that only "true" or "false" are used to denote if an attribute is required.

Number of attributes rule
-------------------------

(NYI)

Limits the number of attributes a single tag file may have.

Forbid optional number attributes rule
--------------------------------------

**Name**: ForbidOptionalNumberAttributes

This rule detects if optional attributes of with a type that is Integer, Float, Double, Short or Char is used. This is 
considered dangerous because setting these attributes to an empty string sets the values to 0, which is not what the 
user might expect, especially when nesting tag file usage.

Forbid to set rtexprvalue on attribute directive to default value explicitly rule
---------------------------------------------------------------------------------

**Name**:DoNotSetRtexprvalueExplicitlyToTrue

This rule finds places where the rtexprvalue has been explicitly set to "true". You may activate this rule to remove 
unneeded clutter from tag files.